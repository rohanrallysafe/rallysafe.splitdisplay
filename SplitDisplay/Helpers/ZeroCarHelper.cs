﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitDisplay.Helpers {
    public class ZeroCar {

        public static int? DigitsToNumber(string digits) {
            if (digits == null) {
                return (int?)null;
            } else if (digits.Equals("000")) {
                return 1023;
            } else if (digits.Equals("00")) {
                return 1022;
            } else if (digits.Equals("0")) {
                return 0;
            } else {
                return Int32.TryParse(digits, out int number) ? number : default(int?);
            }
        }

        public static string NumberToDigits(int number) {
            switch (number) {
                case 1023: return "000";
                case 1022: return "00";
                default: return number.ToString();
            }
        }
    }
}
