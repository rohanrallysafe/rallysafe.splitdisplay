﻿using System.Windows;
using System.Windows.Media;

namespace SplitDisplay.Helpers {
    internal class Styles {
        static internal Style Default = (Style)Application.Current.Resources["ListText"];
        static internal Style Contacting = (Style)Application.Current.Resources["ContactingListText"];
        static internal Style NoReply = (Style)Application.Current.Resources["NoReplyListText"];
        static internal Style Reply = (Style)Application.Current.Resources["ListText"];
        static internal Style Warning = (Style)Application.Current.Resources["NoReplyListText"];
        static internal Style Error = (Style)Application.Current.Resources["JumpStartText"];
    }

    internal class Battery {
        static internal PathGeometry Charging = (PathGeometry)Application.Current.Resources["BatteryCharging"];
        static internal PathGeometry High = (PathGeometry)Application.Current.Resources["BatteryHigh"];
        static internal PathGeometry Medium = (PathGeometry)Application.Current.Resources["BatteryMedium"];
        static internal PathGeometry Low = (PathGeometry)Application.Current.Resources["BatteryLow"];
        static internal PathGeometry Critical = (PathGeometry)Application.Current.Resources["BatteryCritical"];
    }
}
