﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SplitDisplay.Helpers {

    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public class BooleanImageConverter : MarkupExtension, IValueConverter {
        private static BooleanImageConverter _converter = null;

        public override object ProvideValue(IServiceProvider serviceProvider) {
            if (_converter == null) _converter = new BooleanImageConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            ImageSource result = null;
            bool boolValue = (bool)value;
            if (boolValue) {
                result = new BitmapImage(new Uri("/images/tick.png", UriKind.Relative));
            } else {
                result = new BitmapImage(new Uri("/images/delete.png", UriKind.Relative));
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public class DateTimeConverter : MarkupExtension, IValueConverter {
        private static DateTimeConverter _converter = null;

        public DateTimeConverter() {
            // Do nothing but suppress warnings
        }

        public override object ProvideValue(IServiceProvider serviceProvider) {
            if (_converter == null) _converter = new DateTimeConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value != null) {
                DateTime? dt = null;
                if (value is DateTime?) {
                    dt = (DateTime?)value;
                } else if (value is DateTime) {
                    dt = (DateTime)value;
                }

                if (dt.HasValue) {
                    if (!(parameter is string)) {
                        parameter = "hh:mm:ss.f";
                    }
                    return dt.Value.ToString((string)parameter);
                }
            }

            return String.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public class TimeSpanConverter : MarkupExtension, IValueConverter {
        private static TimeSpanConverter _converter = null;

        public TimeSpanConverter() {
            // Do nothing but suppress warnings
        }

        public override object ProvideValue(IServiceProvider serviceProvider) {
            if (_converter == null) {
                _converter = new TimeSpanConverter();
            }
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value != null) {
                TimeSpan? ts = null;
                if (value is TimeSpan?) {
                    ts = (TimeSpan?)value;
                } else if (value is TimeSpan) {
                    ts = (TimeSpan)value;
                }

                if (ts.HasValue) {
                    if (!(parameter is string)) {
                        parameter = @"hh\:mm\:ss";
                    }
                    return ts.Value.ToString((string)parameter);
                }
            }

            return String.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            //throw new NotImplementedException();
            return value;
        }
    }

    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public class VisibilityConverter : MarkupExtension, IValueConverter {

        private static VisibilityConverter _converter = null;

        public override object ProvideValue(IServiceProvider serviceProvider) {
            if (_converter == null) _converter = new VisibilityConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            bool isVisible = (bool)value;

            // If visibility is inverted by the converter parameter, then invert our value
            if (IsVisibilityInverted(parameter))
                isVisible = !isVisible;

            return (isVisible ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            bool isVisible = ((Visibility)value == Visibility.Visible);

            // If visibility is inverted by the converter parameter, then invert our value
            if (IsVisibilityInverted(parameter))
                isVisible = !isVisible;

            return isVisible;
        }

        private static Visibility GetVisibilityMode(object parameter) {
            // Default to Visible
            Visibility mode = Visibility.Visible;

            // If a parameter is specified, then we'll try to understand it as a Visibility value
            if (parameter != null) {
                // If it's already a Visibility value, then just use it
                if (parameter is Visibility) {
                    mode = (Visibility)parameter;
                } else {
                    // Let's try to parse the parameter as a Visibility value, throwing an exception when the parsing fails
                    try {
                        mode = (Visibility)Enum.Parse(typeof(Visibility), parameter.ToString(), true);
                    } catch (FormatException e) {
                        throw new FormatException("Invalid Visibility specified as the ConverterParameter.  Use Visible or Collapsed.", e);
                    }
                }
            }

            // Return the detected mode
            return mode;
        }

        private static bool IsVisibilityInverted(object parameter) {
            return (GetVisibilityMode(parameter) == Visibility.Collapsed);
        }
    }
}
