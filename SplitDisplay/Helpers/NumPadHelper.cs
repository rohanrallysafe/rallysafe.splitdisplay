﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SplitDisplay.Helpers {
    public class NumPadHelper {

        public static string GetNumpadDigits(string number, string digits, int length) {
            if (!String.IsNullOrEmpty(number) && !String.IsNullOrEmpty(digits) && digits.Length < length) {
                return String.Concat(digits, number);
            } else {
                return number;
            }
        }
    }
}
