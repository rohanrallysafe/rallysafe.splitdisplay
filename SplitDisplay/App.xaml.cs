﻿using RallySafe.Radio;
using Microsoft.Win32;
using SplitDisplay.Communication;
using SplitDisplay.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Deployment.Application;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Utilities;

namespace SplitDisplay {

    public enum ApplicationState { Default, AToA }

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {

        private DuplicateInstanceMonitor _instanceMonitor;
        private MainWindow _window;

        protected override void OnStartup(StartupEventArgs e) {
            base.OnStartup(e);

            try {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\SplitDisplay\\";
                if (!Directory.Exists(path)) {
                    Directory.CreateDirectory(path);
                }
                AppDomain.CurrentDomain.SetData("DataDirectory", path);

                // Detect if we are a duplicate instance
                _instanceMonitor = Utilities.Windows.CreateDuplicateInstanceMonitor();
                if (_instanceMonitor == null) {
                    Application.Current.Shutdown();
                } else {
                    Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
                    _instanceMonitor.DuplicateInstanceLaunched += (sender, args) => dispatcher.Invoke(() => _window.Activate());
                }

                Utilities.Windows.DisableCharmsBar();
                Application.Current.DispatcherUnhandledException += OnUnhandledException;

                Time time = new Time();
                var now = time.Now;

                // Trace execution and listen for crashes
                string filename = Path.Combine(path, $"SplitDisplay-{now.Year}.{now.Month}.{now.Day}-Trace.log");
                Trace.Listeners.Add(new TextWriterTraceListener(filename));
                Trace.TraceInformation("----- RallySafe SplitDisplay starting -----");
                Trace.TraceInformation($"{DateTime.Now.ToLongTimeString()}");
                string version;
                try {
                    version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                } catch {
                    version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }

                Trace.TraceInformation($"Version: {version}");
                Trace.Flush();

                IRallySafeRadioInterface radio = new RallySafeRadioInterface(time);
                //IRallySafeRadioInterface radio = new DebugRallySafeRadioInterface(time);

                ApplicationViewModel model = new ApplicationViewModel(radio, time);
                _window = new MainWindow();
                _window.VM = model;
                _window.Show();
            } catch (Exception x) {
                Trace.TraceError(x.Message);
                throw;
            }
        }

        private void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e) {
            Trace.TraceError(e.Exception.Message);
            Trace.TraceError(e.Exception.StackTrace);
            Trace.Flush();
        }

        protected override void OnExit(ExitEventArgs e) {
            if (_instanceMonitor != null) {
                _instanceMonitor.Dispose();
            }

            Utilities.Windows.EnableCharmsBar();
            Trace.Flush();
            base.OnExit(e);
        }
    }
}
