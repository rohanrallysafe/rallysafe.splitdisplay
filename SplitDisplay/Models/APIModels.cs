﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitDisplay.Models {

    public class EventAPIModel {
        public int EventId { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public double TimeZone { get; set; }
        public string UseBeamTime { get; set; }
    }

    public class StageAPIModel {
        public int StageId { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
    }

    public class LocationPointAPIModel {
        public int LocationPointId { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string LocationPointTypeCode { get; set; }
        public string LocationPointTypeName { get; set; }
        public string Name { get; set; }
        public int? TimeControlId { get; set; }
    }

    public class SubmitControlTime {
        public int EventId { get; set; }
        public int TimeControlId { get; set; }
        public ControlTime[] ControlTimes { get; set; }
    }

    public class ControlTime {
        public int CarNumber { get; set; }
        public DateTime? OfficialCheckInTime { get; set; }
        public DateTime? RequestedCheckInTime { get; set; }
    }
}
