﻿using SplitDisplay.Data;
using RallySafe.Radio.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SplitDisplay.Models {
    public class MessageEvent {
        public string Title { get; set; }
        public string Message { get; set; }
    }

    public class ConfirmationEvent : MessageEvent {
        public bool Confirmed { get; set; }
    }

    public class ConfirmStageEvent {
        public int CarNumber { get; set; }
        public ObservableCollection<Stage> Stages { get; set; }
        public Stage Stage { get; set; }
        public ICommand ConfirmCommand { get; set; }
    }

}
