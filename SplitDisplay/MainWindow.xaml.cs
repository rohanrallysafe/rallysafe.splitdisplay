﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SplitDisplay.ViewModels;

namespace SplitDisplay {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private ApplicationViewModel _vm;

        public ApplicationViewModel VM {
            set {
                _vm = value;
                this.DataContext = _vm;
            }
        }

        public MainWindow() {
            InitializeComponent();
        }
    }
}
