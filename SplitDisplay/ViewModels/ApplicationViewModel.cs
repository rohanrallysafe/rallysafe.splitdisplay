﻿using RallySafe.Radio.Messages;
using SplitDisplay.Commands;
using SplitDisplay.Communication;
using SplitDisplay.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Utilities;
using System.Diagnostics;
using Microsoft.Win32;
using System.ComponentModel;
using System.Windows.Data;

namespace SplitDisplay.ViewModels {
    public class ApplicationViewModel : BindableBase {

        private SplitDisplayContainer _db;

        public ICommand QuitCommand { get; set; }
        public ICommand ClearDataCommand { get; set; }

        public ObservableCollection<SplitTimeViewModel> SplitTimes { get; set; }
        public ICollectionView Splits { get; set; }

        private Style _black;
        private Style _blue;

        private IRallySafeRadioInterface _radio;

        private Time _time;  // The ultimate source of time
        private Timer _clockTimer;  // Update the clock
        private Timer _dbTimer; // Save database at regular intervals
        private Timer _batteryTimer;  // Update the battery status

        public bool TimeControlVisibility { get; protected set; }

        private Page currentPage;
        public Page CurrentPage {
            get { return currentPage; }
            set { SetProperty(ref currentPage, value); }
        }

        private string _title;
        public string Title {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string clock;
        public string Clock {
            get { return clock; }
            set { SetProperty(ref clock, value); }
        }

        private DateTime _clockTime;
        public DateTime ClockTime {
            get { return _clockTime; }
            set { SetProperty(ref _clockTime, value); }
        }

        private string battery;
        public string Battery {
            get { return battery; }
            set { SetProperty(ref battery, value); }
        }

        private double _batteryPercent;
        public double BatteryPercent {
            get { return _batteryPercent; }
            set { SetProperty(ref _batteryPercent, value); }
        }

        private PathGeometry batteryIcon;
        public PathGeometry BatteryIcon {
            get { return batteryIcon; }
            set { SetProperty(ref batteryIcon, value); }
        }

        bool _isRadioConnected;
        public bool IsRadioConnected {
            get { return _isRadioConnected; }
            set { SetProperty(ref _isRadioConnected, value); }
        }


        private string radioTypeText;
        public string RadioTypeText {
            get { return radioTypeText; }
            set {
                radioTypeText = value;
                OnPropertyChanged("RadioTypeText");
            }
        }

        public ApplicationViewModel(IRallySafeRadioInterface radio, Time time) {
            _db = new SplitDisplayContainer();

            _time = time;

            SplitTimes = new AsyncObservableCollection<SplitTimeViewModel>();
            Splits = CollectionViewSource.GetDefaultView(SplitTimes);
            Splits.SortDescriptions.Clear();
            Splits.SortDescriptions.Add(new SortDescription("Split", ListSortDirection.Descending));

            BindCommands();

            // Bind Styles
            _black = (Style)Application.Current.Resources["blackFlagStyle"];
            _blue = (Style)Application.Current.Resources["blueFlagStyle"];

            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(SystemEvents_PowerModeChanged);
            SetBatteryIcon();


            // Connect to radio
            Trace.TraceInformation("Connecting to Radio");
            Trace.Flush();
            _radio = radio;
            _radio.Connected += OnRadioConnected;
            _radio.Disconnected += () => IsRadioConnected = false;

            IsRadioConnected = _radio.IsConnected;
            _radio.ReceiveSplitTime += OnReceiveSplitTime;
            StartClock();

            foreach (var sp in _db.SplitTimes) {
                SplitTimes.Add(new SplitTimeViewModel(sp, _time));
            }
        }

        void OnRadioConnected(RadioType radioType) {
            IsRadioConnected = true;
            switch (radioType) {
                case RadioType.Rfd868:
                case RadioType.Rfd900:
                    RadioTypeText = "Radio Connected";
                    break;
                case RadioType.Xbee:
                    RadioTypeText = "XBee Connected";
                    break;
                default:
                    RadioTypeText = "Unknown Radio";
                    break;
            }
        }

        private void SetBatteryIcon() {
            var batteryLife = System.Windows.Forms.SystemInformation.PowerStatus.BatteryLifePercent;
            if (System.Windows.Forms.SystemInformation.PowerStatus.PowerLineStatus == System.Windows.Forms.PowerLineStatus.Online) {
                BatteryIcon = Helpers.Battery.Charging;
            } else if (batteryLife > 0.77) {
                BatteryIcon = Helpers.Battery.High;
            } else if (batteryLife > 0.33) {
                BatteryIcon = Helpers.Battery.Medium;
            } else if (batteryLife > 0.05) {
                BatteryIcon = Helpers.Battery.Low;
            } else {
                BatteryIcon = Helpers.Battery.Critical;
            }
        }

        void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e) {
            if (e.Mode == PowerModes.StatusChange) {
                SetBatteryIcon();
            }
        }

        private void BindCommands() {
            QuitCommand = new ActionCommand((param) => Quit());
            ClearDataCommand = new ActionCommand((param) => ClearData());
        }

        private void OnReceiveSplitTime(RallySafe.Radio.Messages.SplitTime splitTime) {
            var sp = _db.SplitTimes.Add(_db.SplitTimes.Create());
            sp.CarNumber = splitTime.Car;
            sp.Split = splitTime.Split;
            sp.Stage = splitTime.Stage;
            var model = new SplitTimeViewModel(sp, _time);
            SplitTimes.Add(model);
            Splits.Refresh();
        }
        private void StartClock() {
            // Update the clock every so often, save database changes every 10 seconds
            _clockTimer = _time.Every(TimeSpan.FromSeconds(0.2), UpdateClock);
            _dbTimer = _time.Every(TimeSpan.FromSeconds(10), (t) => _db.SaveChanges());
            _batteryTimer = _time.Every(TimeSpan.FromMinutes(1), UpdateBattery);
        }

        private void UpdateClock(DateTime time) {
            Clock = time.ToString("HH:mm:ss");
            ClockTime = time;
        }

        private void UpdateBattery(DateTime time) {
            Battery = string.Format("{0}%", System.Windows.Forms.SystemInformation.PowerStatus.BatteryLifePercent * 100);
            SetBatteryIcon();
        }

        public void Quit() {
            if (MessageBox.Show("Do you want to quit?", "Quit", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                _db.SaveChanges();
                Application.Current.Shutdown();
            }
        }

        public void ClearData() {
            if (MessageBox.Show("Do you want to remove all data?", "Clear Data", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                SplitTimes.Clear();
                _db.ClearData();
            }
        }

    }
}
