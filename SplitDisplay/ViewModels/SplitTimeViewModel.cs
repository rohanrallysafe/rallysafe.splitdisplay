﻿using SplitDisplay.Data;
using SplitDisplay.Helpers;
using RallySafe.Radio.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Utilities;

namespace SplitDisplay.ViewModels {
    public class SplitTimeViewModel : BindableBase {

        public Data.SplitTime SplitTime { get; set; }
        private Time TimeSource { get; set; }

        public SplitTimeViewModel(Data.SplitTime splitTime, Time timeSource) {
            SplitTime = splitTime;
            TimeSource = timeSource;
            Split = splitTime.Split.Value;
            CarNumber = SplitTime.CarNumber;
            StageNumber = SplitTime.Stage;
            SplitTimeText = SplitTime.Split.Value.ToString("HH:mm:ss.f");

            Style = Styles.Default;
        }

        int? _carNumber;
        public int? CarNumber {
            get { return _carNumber; }
            set {
                SetProperty(ref _carNumber, value);
                if (_carNumber.HasValue) {
                    CarNumberText = ZeroCar.NumberToDigits(_carNumber.Value);
                } else {
                    CarNumberText = null;
                }
            }
        }

        string _carNumberText;
        public string CarNumberText {
            get { return _carNumberText; }
            set { SetProperty(ref _carNumberText, value); }
        }

        int? _stage;
        public int? StageNumber {
            get { return _stage; }
            set { SetProperty(ref _stage, value); }
        }

        DateTime _split;
        public DateTime Split {
            get { return _split; }
            set { SetProperty(ref _split, value); }
        }

        string _splitTimeText;
        public string SplitTimeText {
            get { return _splitTimeText; }
            set { SetProperty(ref _splitTimeText, value); }
        }

        //TimeSpan _secondsSinceFinish;
        //public TimeSpan SecondsSinceFinish {
        //    get { return _secondsSinceFinish; }
        //    set { SetProperty(ref _secondsSinceFinish, value); }
        //}

        Style _style;
        public Style Style {
            get { return _style; }
            set { SetProperty(ref _style, value); }
        }

        //public void UpdateTimeSinceFinish(DateTime time) {
        //    SecondsSinceFinish = time.Subtract(Time);
        //}

        public void ReceiveCarSplitTimeTenth(RallySafe.Radio.Messages.SplitTime splitTime) {
            // Just save the values for now, don't report them

            SplitTime.Split = splitTime.Split;
            SplitTime.Stage = splitTime.Stage;
            SplitTime.CarNumber = splitTime.Car;

            SplitTimeText = SplitTime.Split.Value.ToString("HH:mm:ss.f");
        }
    }
}
