﻿using SplitDisplay.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Utilities;

namespace SplitDisplay.ViewModels {

    /// <summary>
    /// A model for a time source that increments as it elapses.
    /// </summary>
    public class IncrementingTime : BindableBase {

        private Time _timeSource;
        private Timer _timer;
        private bool _update;

        public IncrementingTime(Time source) {
            _timeSource = source;
            _timeSource.Synchronised += OnTimeSynchronised;

            Delta = TimeSpan.FromSeconds(30);
            Time = _timeSource.RoundTo(Delta);
            AllowOffsetInPast = false;

            // Check whether we need to update the time based on whether it has elapsed
            _timer = _timeSource.Every(TimeSpan.FromSeconds(1), OnTimeElapsed);
            _update = true;
        }

        void OnTimeSynchronised(TimeSpan delta) {
            OnTimeElapsed(_timeSource.Now);
        }

        private DateTime _time;
        public DateTime Time {
            get { return _time; }
            set { SetProperty(ref _time, value); }
        }

        TimeSpan _delta;
        public TimeSpan Delta {
            get { return _delta; }
            set {
                SetProperty(ref _delta, value);
                Time = _timeSource.RoundTo(value);
            }
        }

        public bool AllowOffsetInPast {
            get; set;
        }

        public void Offset(TimeSpan amount) {
            var newTime = Time + amount;
            if (AllowOffsetInPast) {
                Time = newTime;
            } else if (newTime >= (_timeSource.RoundTo(Delta))) {
                Time = newTime;
            }
        }

        public void Pause() {
            _update = false;
        }

        public void Resume() {
            _update = true;
            Time = Utilities.Time.RoundTo(_timeSource.Now, Delta);
        }

        private void OnTimeElapsed(DateTime time) {
            if (_update) {
                var newTime = Utilities.Time.RoundTo(time, Delta);
                if (newTime > Time) {
                    Time = newTime;
                }
            }
        }
    }
}
