﻿using SplitDisplay.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SplitDisplay.Commands {
    public class QuitCommand : ICommand {

        private readonly ApplicationViewModel _vm;

        public QuitCommand(ApplicationViewModel vm) {

            _vm = vm;
        }

        public bool CanExecute(object parameter) {
            // You can always quit
            return true;
        }

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter) {
            _vm.Quit();
        }
    }
}
