﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SplitDisplay.Commands {
    public static class DataGrid {

        public static readonly DependencyProperty OnDoubleClickProperty =
          DependencyProperty.RegisterAttached("OnDoubleClick", typeof(ICommand), typeof(DataGrid),
                            new PropertyMetadata(new PropertyChangedCallback(AttachOrRemoveDataGridDoubleClickEvent)));

        public static ICommand GetOnDoubleClick(DependencyObject obj) {
            return (ICommand)obj.GetValue(OnDoubleClickProperty);
        }

        public static void SetOnDoubleClick(DependencyObject obj, ICommand value) {
            obj.SetValue(OnDoubleClickProperty, value);
        }

        public static void AttachOrRemoveDataGridDoubleClickEvent(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var dataGrid = obj as System.Windows.Controls.DataGrid;
            if (dataGrid != null) {
                if (args.OldValue == null && args.NewValue != null) {
                    dataGrid.MouseDoubleClick += ExecuteDataGridDoubleClick;
                } else if (args.OldValue != null && args.NewValue == null) {
                    dataGrid.MouseDoubleClick -= ExecuteDataGridDoubleClick;
                }
            }
        }

        private static void ExecuteDataGridDoubleClick(object sender, MouseButtonEventArgs args) {
            DependencyObject obj = sender as DependencyObject;
            ICommand cmd = (ICommand)obj.GetValue(OnDoubleClickProperty);
            if (cmd != null) {
                if (cmd.CanExecute(obj)) {
                    cmd.Execute(obj);
                }
            }
        }
    }
}
