﻿using System.Windows;

namespace SplitDisplay {
    interface IKeyListener {
        void BindKeys(UIElement element);
        void UnbindKeys(UIElement element);
    }
}
