﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Utilities;

namespace SplitDisplay.Controls {
    /// <summary>
    /// Interaction logic for NumberPad.xaml
    /// </summary>
    public partial class NumberPad : UserControl {

        #region Dependency properties
        public static readonly DependencyProperty NumberButtonStyleProperty =
            DependencyProperty.Register("NumberButtonStyle", typeof(Style), typeof(NumberPad));

        public Style NumberButtonStyle {
            get { return (Style)GetValue(NumberButtonStyleProperty); }
            set { SetValue(NumberButtonStyleProperty, value); }
        }

        public static readonly DependencyProperty ControlButtonStyleProperty =
            DependencyProperty.Register("ControlButtonStyle", typeof(Style), typeof(NumberPad));

        public Style ControlButtonStyle {
            get { return (Style)GetValue(ControlButtonStyleProperty); }
            set { SetValue(ControlButtonStyleProperty, value); }
        }

        public static readonly DependencyProperty DigitsProperty =
            DependencyProperty.Register("Digits", typeof(string), typeof(NumberPad));

        public string Digits {
            get { return (string)GetValue(DigitsProperty); }
            set { SetValue(DigitsProperty, value); }
        }

        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(int), typeof(NumberPad), new PropertyMetadata { DefaultValue = 3 });

        public int Length {
            get { return (int)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        public static readonly DependencyProperty PlusCommandProperty =
            DependencyProperty.Register("PlusCommand", typeof(ICommand), typeof(NumberPad), new PropertyMetadata { DefaultValue = default(ICommand) });
        public ICommand PlusCommand {
            get { return (ICommand)GetValue(PlusCommandProperty); }
            set { SetValue(PlusCommandProperty, value); }
        }

        public static readonly DependencyProperty MinusCommandProperty =
            DependencyProperty.Register("MinusCommand", typeof(ICommand), typeof(NumberPad), new PropertyMetadata { DefaultValue = default(ICommand) });
        public ICommand MinusCommand {
            get { return (ICommand)GetValue(MinusCommandProperty); }
            set { SetValue(MinusCommandProperty, value); }
        }

        public static readonly DependencyProperty NumberEnteredProperty =
            DependencyProperty.Register("NumberEntered", typeof(ICommand), typeof(NumberPad), new PropertyMetadata { DefaultValue = default(ICommand) });

        public ICommand NumberEntered {
            get { return (ICommand)GetValue(NumberEnteredProperty); }
            set { SetValue(NumberEnteredProperty, value); }
        }
        #endregion

        public ICommand ButtonCommand { get; protected set; }

        public NumberPad() {
            InitializeComponent();
            (Content as FrameworkElement).DataContext = this;  // Assign context to first item, not root

            ButtonCommand = new ActionCommand(ButtonPressed);
            AddKeyBinding(Key.D0, "0");
            AddKeyBinding(Key.NumPad0, "0");
            AddKeyBinding(Key.D1, "1");
            AddKeyBinding(Key.NumPad1, "1");
            AddKeyBinding(Key.D2, "2");
            AddKeyBinding(Key.NumPad2, "2");
            AddKeyBinding(Key.D3, "3");
            AddKeyBinding(Key.NumPad3, "3");
            AddKeyBinding(Key.D4, "4");
            AddKeyBinding(Key.NumPad4, "4");
            AddKeyBinding(Key.D5, "5");
            AddKeyBinding(Key.NumPad5, "5");
            AddKeyBinding(Key.D6, "6");
            AddKeyBinding(Key.NumPad6, "6");
            AddKeyBinding(Key.D7, "7");
            AddKeyBinding(Key.NumPad7, "7");
            AddKeyBinding(Key.D8, "8");
            AddKeyBinding(Key.NumPad8, "8");
            AddKeyBinding(Key.D9, "9");
            AddKeyBinding(Key.NumPad9, "9");
            AddKeyBinding(Key.Add, "+");
            AddKeyBinding(Key.Subtract, "-");
            AddKeyBinding(Key.Enter, "E");
            AddKeyBinding(Key.Back, "X");
            AddKeyBinding(Key.Delete, "X");
        }

        private void ButtonPressed(object param) {
            if (param == null) {
                Digits = null;
            } else {
                if (param.Equals("X")) {
                    Digits = null;
                } else if (param.Equals("E")) {
                    NumberEntered?.Execute(Digits);
                } else if (param.Equals("+")) {
                    PlusCommand?.Execute(null);
                } else if (param.Equals("-")) {
                    MinusCommand?.Execute(null);
                } else if (Int32.TryParse(param.ToString(), out int number)) {
                    if (Digits != null && Digits.Length < Length) {
                        Digits = String.Concat(Digits, number);
                    } else {
                        Digits = number.ToString();
                    }
                }
            }
        }

        #region KeyBindings
        private List<KeyBinding> KeyBindings { get; } = new List<KeyBinding>();
        private void AddKeyBinding(Key key, string text) {
            KeyBindings.Add(new KeyBinding { Key = key, Command = new ActionCommand((param) => ButtonPressed(text)) });
        }
        public void BindKeys(UIElement element) {
            foreach (var binding in KeyBindings) {
                element.InputBindings.Add(binding);
            }
        }
        public void UnbindKeys(UIElement element) {
            foreach (var binding in KeyBindings) {
                element.InputBindings.Remove(binding);
            }
        }
        #endregion
    }
}
