﻿using System;

namespace SplitDisplay.Data {
    public class SplitTime {
        public int SplitTimeId { get; set; }
        public int? CarNumber { get; set; }
        public DateTime? Split { get; set; }
        public int? Stage { get; set; }
    }
}
