﻿using System;

namespace SplitDisplay.Data {
    public class TimeControlTime {
        public int TimeControlTimeId { get; set; }
        public int CarNumber { get; set; }
        public DateTime? RequestedCheckInTime { get; set; }
        public DateTime? OfficialCheckInTime { get; set; }
        public virtual TimeControl TimeControl { get; set; }
    }
}
