﻿using System;
using System.Collections.Generic;

namespace SplitDisplay.Data {
    public partial class TimeControl {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimeControl() {
            this.Times = new HashSet<TimeControlTime>();
        }

        public int TimeControlId { get; set; }
        public string Name { get; set; }
        public Nullable<int> TimeControlNumber { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeControlTime> Times { get; set; }
    }
}
