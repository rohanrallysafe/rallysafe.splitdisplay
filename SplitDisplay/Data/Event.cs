﻿using System;

namespace SplitDisplay.Data {
    public partial class Event {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string Name { get; set; }
        public DateTime SyncDate { get; set; }
        public bool UseBeamTimes { get; set; }
    }
}
