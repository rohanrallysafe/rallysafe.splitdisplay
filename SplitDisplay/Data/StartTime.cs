﻿using System;

namespace SplitDisplay.Data {
    public class StartTime {
        public int StartTimeId { get; set; }
        public int CarNumber { get; set; }
        public DateTime? IssuedStartTime { get; set; }
        public DateTime? IssuedStartSent { get; set; }
        public DateTime? IssuedStartAcknowledged { get; set; }
        public DateTime? ActualStartTime { get; set; }
        public DateTime? BeamBreakTime { get; set; }
        public int Problem { get; set; }
        public bool IssuedStartPrinted { get; set; }
        public bool BeamBreakPrinted { get; set; }
        public virtual Stage Stage { get; set; }
    }
}
