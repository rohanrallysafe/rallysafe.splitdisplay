﻿using System.Collections.Generic;

namespace SplitDisplay.Data {
    public partial class Stage {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Stage() {
            this.StartTimes = new HashSet<StartTime>();
            this.FinishTimes = new HashSet<SplitTime>();
        }

        public int Id { get; set; }
        public int StageId { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StartTime> StartTimes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SplitTime> FinishTimes { get; set; }
    }
}
