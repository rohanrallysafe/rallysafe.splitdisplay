﻿using SplitDisplay.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.CodeFirst;

namespace SplitDisplay.Data {
    public partial class SplitDisplayContainer : DbContext {

        public DbSet<SplitTime> SplitTimes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            var sqlite = new SqliteDropCreateDatabaseWhenModelChanges<SplitDisplayContainer>(modelBuilder);
            Database.SetInitializer(sqlite);
        }

        public void ClearData() {
            SplitTimes.RemoveRange(SplitTimes.ToArray());
            SaveChanges();

        }
    }
}
