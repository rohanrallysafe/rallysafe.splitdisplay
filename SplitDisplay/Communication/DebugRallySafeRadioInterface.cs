﻿using SplitDisplay.ViewModels;
using RallySafe.Radio.Messages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;
using Utilities;

namespace SplitDisplay.Communication {
    internal class DebugRallySafeRadioInterface : IRallySafeRadioInterface {
        public event Action<RadioType> Connected;
        public event Action Disconnected;
        // Asynchronous messages
        public event Action<SplitTime> ReceiveSplitTime;

        public bool SyncTime { get; set; }

        private readonly Time _time;
        private readonly Dispatcher _dispatcher;
        private readonly HashSet<int> _setToStage = new HashSet<int>();

        public DebugRallySafeRadioInterface(Time time) {
            _time = time;
            _dispatcher = Dispatcher.CurrentDispatcher;
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(SendBeamBreak);
            aTimer.Interval = 5000;
            aTimer.Enabled = true;

            // Connect the radio after 2 seconds
            Task.Run(async () => {
                await Task.Delay(2000);
                Connected?.Invoke(RadioType.Rfd900);
            });

            Task.Run(async () => {
                while (true) {
                    await Task.Delay(5000);
                    await Splits();
                }
            });


        }

        public bool IsConnected {
            get { return true; }
        }

        public async Task CheckSynchronise(int serial) {

        }

        public void SendArriveAtCheckInAck(int car) {
            // Do nothing
        }

        public Task<bool> SendCancelStartTime(int car, int stage) {


            return Task.FromResult(true);
        }

        public void SendFinishAck(int imei) { }

        public Task<bool> SendMoveToStage(int car, int stage) {
            return Task.FromResult(true);
        }

        public void SendRequestedCheckInAck(int car) {
        }

        public void SendStartAck(int car) { }


        private void SendBeamBreak(object source, ElapsedEventArgs e) {
            //var time = DateTime.Now;
            //Random rnd = new Random();
            //Application.Current.Dispatcher.Invoke(() => {
            //    BeamBreak b = new BeamBreak();
            //    b.Car = 0;
            //    b.Stage = 6;
            //    b.Unit = 0;
            //    b.Time = time;
            //    ReceiveBeamBreak?.Invoke(b);
            //});
            //Application.Current.Dispatcher.Invoke(() => {
            //    BeamBreak b2 = new BeamBreak();
            //    b2.Car = rnd.Next(1, 250);
            //    b2.Stage = 6;
            //    b2.Unit = rnd.Next(2250, 3800);
            //    b2.Time = time;
            //    ReceiveBeamBreak?.Invoke(b2);
            //});
        }

        public Task<TimeSynchronise> SendTimeSynchronise(int car) {
            return Task.FromResult<TimeSynchronise>(null);
        }

        // See the accompanying test plan for the meaning of these methods
        private async Task<bool> NoResult() {
            await Task.Delay(1000);
            return false;
        }


        private Task<bool> Splits() {

            DateTime now = DateTime.Now;
            Random rnd = new Random();
            Task.Run(async () => {
                ReceiveSplitTime?.Invoke(new SplitTime {
                    Car = rnd.Next(01, 100),
                    Stage = 1,
                    Split = now
                });
            });

            return Task.FromResult(false);
        }
    }
}
