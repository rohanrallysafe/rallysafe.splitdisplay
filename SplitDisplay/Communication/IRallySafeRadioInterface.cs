﻿using SplitDisplay.ViewModels;
using RallySafe.Radio.Messages;
using System;
using System.Threading.Tasks;

namespace SplitDisplay.Communication {
    public interface IRallySafeRadioInterface {

        event Action<RadioType> Connected;
        event Action Disconnected;

        event Action<SplitTime> ReceiveSplitTime;


        bool SyncTime { get; set; }
        bool IsConnected { get; }


        Task<TimeSynchronise> SendTimeSynchronise(int car);


        Task CheckSynchronise(int car);
    }
}
