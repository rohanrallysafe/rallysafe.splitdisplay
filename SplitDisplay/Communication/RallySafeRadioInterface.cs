﻿using Hardware.ComPort.Experimental;
using Microsoft.Win32;
using RallySafe.Radio;
using RallySafe.Radio.Messages;
using RallySafe.Radio.Protocols;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows;
using System.Windows.Threading;
using Utilities;
using SplitDisplay.Helpers;
using SplitDisplay.ViewModels;
using System.IO;

namespace SplitDisplay.Communication {
    public enum RadioType {
        Unknown = 0,
        Xbee = 1,
        Rfd900 = 2,
        Rfd868 = 3
    }

    public class RallySafeRadioInterface : IRallySafeRadioInterface {
        // Radio connection events
        public event Action<RadioType> Connected;
        public event Action Disconnected;

        // Asynchronous messages
        public event Action<SplitTime> ReceiveSplitTime;

        // Port management
        private ComPort _port;
        private MessageNetwork _network;
        private CancellationTokenSource _cancellationSource;



        private Time _time;
        public bool SyncTime { get; set; }

        public bool IsConnected => _port != null;

        public RallySafeRadioInterface(Time time) {
            _time = time ?? throw new ArgumentNullException("time");

            // On power mode suspend, disconnect radio
            // SystemEvents.PowerModeChanged += ManageConnection;

            // Start managing the connection
            _cancellationSource = new CancellationTokenSource();
            Task.Run(() => ManageConnection());

            // Ensure everything cleans up nicely at application exit
            Application.Current.Exit += (sender, e) => {
                _cancellationSource.Cancel();
                _port?.Close();
            };
        }

        private void ManageConnection() {
            // Listen for changes to attached ports
            var portCollection = new PortCollection();
            var deviceInserted = new ManualResetEventSlim();
            portCollection.CollectionChanged += (sender, e) => {
                if (e.Action == NotifyCollectionChangedAction.Add) {
                    deviceInserted.Set();
                }
            };

            try {
                var radioType = RadioType.Unknown;
                // Keep cycling connections until cancelled
                while (!_cancellationSource.IsCancellationRequested) {
                    // Try to open an attached device
                    foreach (var p in portCollection.Reverse()) {
                        // Skip bluetooth devices (printers)
                        var driver = PortManager.DriverName(p);
                        if (driver == null || driver.ToLowerInvariant().Contains("bluetooth")) {
                            continue;
                        }

                        // Otherwise, try and detect RFD dongles
                        if (_port == null && PortManager.TryOpen(p, out SerialPort serialPort)) {
                            if (PortManager.Filter("RFD900").Contains(p) || PortManager.Filter("RFD868").Contains(p)) {
                                radioType = RadioType.Rfd900;
                                serialPort.BaudRate = 115200;
                                _port = new ComPort(serialPort);

                               
                                _network = Saslink.XbeeEncapsulatedNetwork(_port.ReadBlock, _port.WriteBlock);
                            } else {
                                radioType = RadioType.Xbee;
                                serialPort.BaudRate = 19200;
                                _port = new ComPort(serialPort);
                                _network = Xbee.XbeeNetwork(_port.ReadBlock, _port.WriteBlock);
                            }
                        }
                    }

                    // Did we succeed in opening a port?
                    if (_port != null) {
                        // Notify we have a connection
                        if (Connected != null) {
                            Trace.TraceInformation($"Opening Port: {_port.Name}");
                            Connected?.Invoke(radioType);

                            // Listen for the port closing (either explicitly or by removing device)
                            var portClosed = new ManualResetEventSlim();
                            _port.Closed += () => portClosed.Set();

                            // Start processing asynchronous incoming messages terminating either on
                            // cancellation, or on the port being forcible closed (yanked)
                            Task.Run(() => {
                                while (!_cancellationSource.IsCancellationRequested && !portClosed.IsSet) {
                                    ReceiveMessage(_network.Source.Receive(_cancellationSource.Token));
                                }
                            });

                            // Wait for the port closing event to trigger
                            portClosed.Wait(_cancellationSource.Token);
                            Disconnected?.Invoke();
                            _port = null;
                            _network = null;
                        }

                    } else {
                        // Wait for a device to be inserted
                        deviceInserted.Reset();
                        deviceInserted.Wait(_cancellationSource.Token);
                    }
                }
            } catch (Exception error) {
                Trace.TraceError($"Exception: {error.Message}");
                Task.Run(() => ManageConnection());
            }

            // We no longer need the port collection
            portCollection.Dispose();
        }

        private void ReceiveMessage(Message message) {
            Application.Current.Dispatcher.Invoke(() => {
                if (message is SplitTime splitTime) {
                    Trace.TraceInformation($"[Received] Split Time: Car={splitTime.Car}, Count={splitTime.Count}, Stage={splitTime.Stage.ToString()}, Split={splitTime.Split.ToString("yyyy-MM-ddTHH:mm:ss.ff")}");
                    ReceiveSplitTime?.Invoke(splitTime);
                } else {
                    Trace.TraceInformation($"[Received] unhandled message: {message.GetType()}");
                }
            });
        }



        private async Task<bool> RetrySend(Message message, int retries) {
            if (_network != null) {
                for (int retry = 0; retry < retries; ++retry) {
                    if (await _network.MessageChannel(message, 600)) {
                        return true;
                    }
                }
                // Failed to send, even with retries
                return false;
            } else {
                return false;
            }
        }

        public async Task<TimeSynchronise> SendTimeSynchronise(int unit) {
            if (_network != null) {
                var msg = new TimeSynchronise { Unit = unit };
                if (await _network.MessageChannel(msg, 1000)) {
                    return msg;
                }
            }
            return null;
        }

        public async Task CheckSynchronise(int serial) {
            if (_network != null && SyncTime) {
                TimeSynchronise message = null;
                DateTime outTimestamp = DateTime.Now;

                // First try to synchronise with clock
                //if (Settings.ClockSerial != 0) {
                //    Trace.TraceInformation($"Synchronising with Clock {Settings.ClockSerial}");
                //    outTimestamp = DateTime.Now;
                //    message = await SendTimeSynchronise(Settings.ClockSerial);
                //}

                // If there is no clock, or the message fails, try to sync with the unit
                if (message == null) {
                    Trace.TraceInformation($"Synchronising with Unit {serial}");
                    outTimestamp = DateTime.Now;
                    message = await SendTimeSynchronise(serial);
                }

                if (message != null) {
                    // A synchronise message has made a round-trip to the unit. Halve
                    // the round-trip time and apply the delta to the returned timestamp.
                    // (Poor-man's NTP).
                    var inTimestamp = DateTime.Now;
                    var delta = (inTimestamp - outTimestamp);

                    var oneWayTripTime = TimeSpan.FromTicks(delta.Ticks / 2L);
                    _time.Synchronise(message.SynchronisedTimeUTC.Add(oneWayTripTime), message.Offset);
                    SyncTime = false;
                }
            }
        }
    }
}
