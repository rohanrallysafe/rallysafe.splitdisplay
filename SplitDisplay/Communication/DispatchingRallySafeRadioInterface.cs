﻿using RadioInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace RaceCommander.Client {
    public class DispatchingRallySafeRadioInterface : IRallySafeRadioInterface {
        private readonly RallySafeRadioInterface _underlying;
        private readonly Dispatcher _currentDispatcher;


        public event Action<StartTimeModel> ReceiveActualStartTime;
        public event Action<ArriveAtTimeControlModel> ReceiveArriveAtTimeControl;
        public event Action<CheckinInAckModel> ReceiveCheckInAck;
        public event Action<IssuedStartTimeAckModel> ReceiveIssuedStartTimeAck;
        public event Action<StageTimeModel> ReceiveFinishTime;
        public event Action<RequestedCheckInModel> ReceiveRequestedCheckIn;
        public event Action<BeamMisalignedModel> ReceiveBeamMisaligned;
        public event Action<MoveToStageAckModel> ReceiveMoveToStageAck;
        public event Action<HazardModel> ReceiveHazard;

        public event Action<TimeSynchronisedModel> TimeSynchronised;
        public event Action<TrafficModel> RadioTraffic;
        public event EventHandler InterfaceConnected;
        public event EventHandler InterfaceDisconnected;

        public DispatchingRallySafeRadioInterface(IRallySafeRadioInterface underlying) {
            _currentDispatcher = Dispatcher.CurrentDispatcher;

            _underlying = underlying;
            _underlying.ReceiveArriveAtTimeControl += new Action<ArriveAtTimeControlModel>(_underlying_ReceiveArriveAtTimeControl);
            _underlying.ReceiveRequestedCheckIn += new Action<RequestedCheckInModel>(_underlying_ReceiveRequestedCheckIn);
            _underlying.ReceiveCheckInAck += new Action<CheckinInAckModel>(_underlying_ReceiveCheckInAck);
            _underlying.ReceiveActualStartTime += new Action<StartTimeModel>(_underlying_ReceiveActualStartTime);
            _underlying.ReceiveIssuedStartTimeAck += new Action<IssuedStartTimeAckModel>(_underlying_ReceiveIssuedStartTimeAck);            
            _underlying.ReceiveFinishTime += new Action<StageTimeModel>(_underlying_ReceiveFinishTime);
            _underlying.ReceiveRequestedCheckIn += new Action<RequestedCheckInModel>(_underlying_ReceiveRequestedCheckIn);
            _underlying.ReceiveBeamMisaligned += new Action<BeamMisalignedModel>(_underlying_ReceiveBeamMisaligned);
            _underlying.ReceiveMoveToStageAck += new Action<MoveToStageAckModel>(_underlying_ReceiveMoveToStageAck);
            _underlying.ReceiveHazard += new Action<HazardModel>(_underlying_ReceiveHazard);

            _underlying.RadioTraffic += new Action<TrafficModel>(_underlying_RadioTraffic);
            _underlying.TimeSynchronised += new Action<TimeSynchronisedModel>(_underlying_TimeSynchronised);
            _underlying.InterfaceConnected += _underlying_InterfaceConnected;
            _underlying.InterfaceDisconnected += _underlying_InterfaceDisconnected;
        }

        #region Events
        
        void _underlying_ReceiveActualStartTime(StartTimeModel obj) {
            Action dispatchAction = () => {
                if (ReceiveActualStartTime != null)
                    ReceiveActualStartTime(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveIssuedStartTimeAck(IssuedStartTimeAckModel obj) {
            Action dispatchAction = () => {
                if (ReceiveIssuedStartTimeAck != null)
                    ReceiveIssuedStartTimeAck(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveFinishTime(StageTimeModel obj) {
            Action dispatchAction = () => {
                if (ReceiveFinishTime != null)
                    ReceiveFinishTime(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveArriveAtTimeControl(ArriveAtTimeControlModel obj) {
            Action dispatchAction = () => {
                if (ReceiveArriveAtTimeControl != null)
                    ReceiveArriveAtTimeControl(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveCheckInAck(CheckinInAckModel obj) {
            Action dispatchAction = () => {
                if (ReceiveCheckInAck != null)
                    ReceiveCheckInAck(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveRequestedCheckIn(RequestedCheckInModel obj) {
            Action dispatchAction = () => {
                if (ReceiveRequestedCheckIn != null)
                    ReceiveRequestedCheckIn(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveMoveToStageAck(MoveToStageAckModel obj) {
            Action dispatchAction = () => {
                if (ReceiveMoveToStageAck != null)
                    ReceiveMoveToStageAck(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveHazard(HazardModel obj) {
            Action dispatchAction = () => {
                if (ReceiveHazard != null)
                    ReceiveHazard(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_ReceiveBeamMisaligned(BeamMisalignedModel obj) {
            Action dispatchAction = () => {
                if (ReceiveBeamMisaligned != null)
                    ReceiveBeamMisaligned(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        // COMMON STUFF

        void _underlying_RadioTraffic(TrafficModel obj) {
            Action dispatchAction = () => {
                if (RadioTraffic != null)
                    RadioTraffic(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_TimeSynchronised(TimeSynchronisedModel obj) {
            Action dispatchAction = () => {
                if (TimeSynchronised != null)
                    TimeSynchronised(obj);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_InterfaceConnected(object sender, EventArgs e) {
            Action dispatchAction = () => {
                if (InterfaceConnected != null)
                    OnInterfaceConnected(sender, e);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        void _underlying_InterfaceDisconnected(object sender, EventArgs e) {
            Action dispatchAction = () => {
                if (InterfaceDisconnected != null)
                    OnInterfaceDisconnected(sender, e);
            };

            _currentDispatcher.BeginInvoke(DispatcherPriority.DataBind, dispatchAction);
        }

        protected virtual void OnInterfaceConnected(object sender, EventArgs e) {
            EventHandler handler = InterfaceConnected;
            if (handler != null) {
                handler(this, e);
            }
        }


        protected virtual void OnInterfaceDisconnected(object sender, EventArgs e) {
            EventHandler handler = InterfaceDisconnected;
            if (handler != null) {
                handler(this, e);
            }
        }

        #endregion


        #region Sending

        public void ChangeBaudRate(int baudRate) {
            _underlying.ChangeBaudRate(baudRate);
        }

        public void AddMessage(RadioMessage message) {
            _underlying.AddMessage(message);
        }

        public void AddMessage(RadioMessage message, RadioInterface.Internal.RadioPriority priority) {
            _underlying.AddMessage(message, priority);
        }

        public void AddMessageGroup(RadioMessageGroup group) {
            _underlying.AddMessageGroup(group);
        }

        public void AddMessageGroup(RadioMessageGroup group, RadioInterface.Internal.RadioPriority priority) {
            _underlying.AddMessageGroup(group, priority);
        }

        public int GetMessageGroupCount() {
            return _underlying.GetMessageGroupCount();
        }

        public bool IsConnected() {
            return _underlying.IsConnected();
        }

        #endregion
    }
}
