This project ships with a SQLServer 2014 database. The deployment package needs to install the SQLServer redistributable. Microsoft does not make a redistributable available as a pre-requisite. This one is available on GitHub:
https://github.com/kjbartel/SqlLocalDB2014-Bootstrapper/

Extract the archive to the C:\Program Files (x86)\Microsoft Visual Studio 14.0\SDK\Bootstrapper\Packages folder and restart Visual Studio. From the project properties, select Publish and then Pre-requisites, and the SQLServer 2014 options should be available. Tick these and click Publish now.
